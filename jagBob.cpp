#include <cstdio>
#include <cstring>
#include "jagBob.h"
#include "jagModel3.h"
#include "jagState3.h"
#include "jagAction3.h"
#include "jagProblem3.h"

namespace jag
{
	namespace bb
	{
		/* For more on options look at the files:
		 * ai-lib/include/Agent/Options.h
		 * ai-lib/src/Agent/Options.cpp
		 * al-lib/src/Agent/AgentDriver.cpp
		 */
		/*
		 * Run with:
		 * ./RunProg ./SA_Test -a s -U 1
		 */
		Bob::Bob(ai::Agent::Options *opts)
		: alg_type(ALG_GRAPH),
		  fringe_type(FRINGE_BFS),
		  max_depth(0), goalHomeMode(0), 
          look_direction(0), new_cell(0),
          first_cell(1),at_home(0),game_over(false),parse_hundred(0),hundred(0),
          deposit_mode(false),got_neg(false),pickup_counter(0),in_bag(0)
		{
			SetName("Bob");
			this->mModel = new Model();
			if (opts->GetArgInt("user1") > 0)
			{
				this->alg_type = opts->GetArgInt("user1");
			}
			if (opts->GetArgInt("user2") > 0)
			{
				this->fringe_type = opts->GetArgInt("user2");
			}
			if (opts->GetArgInt("user3") > 0)
			{
				this->max_depth = opts->GetArgInt("user3");
			}
			if(this->alg_type <= ALG_MIN || this-> alg_type >= ALG_MAX ||
			   this->fringe_type <= FRINGE_MIN || this-> fringe_type >= FRINGE_MAX)
			{
				std::cout << "Usage: " << std::endl
					  << "-U num : "<<ALG_TREE<<" = Tree, "<<ALG_GRAPH<<" = Graph, "
					  << "-V num : "<<FRINGE_BFS<<" = BFS, "<<FRINGE_UCS<<" = UCS, "
					  << "" <<FRINGE_DFS<<" = DFS, " <<FRINGE_DLS<<" = DLS, "
					  <<FRINGE_IDS<<" = IDS, "<< std::endl
					  << "-W num : Max depth for DLS or IDS" << std::endl << std::endl;
			}
            //make vectors to stuff in vectors
            std::vector<std::string> shape;
            std::vector<std::string> size;
            std::vector<std::string> color;

            primative_store.push_back(shape);
            primative_store.push_back(size);
            primative_store.push_back(color);
            //same for mimic
            
            mimic_store.push_back(shape);
            mimic_store.push_back(size);
            mimic_store.push_back(color);
            //same for junk
            junk_store.push_back(shape);
            junk_store.push_back(size);
            junk_store.push_back(color);
		}

		Bob::~Bob()
		{
			if(this->mModel)
			{
				delete this->mModel;
				this->mModel = 0;
			}
		}

		ai::Agent::Action * Bob::Program(const ai::Agent::Percept * percept)
		{
			ai::Scavenger::Action *action = new ai::Scavenger::Action;
            if (this->first_cell == 1)
            {
                //std::cout << "This is the first cell, should happen once" << std::endl;
                Cell firstC = Cell(0.00,0.00,0.00,I_ND,I_ND,I_ND,I_ND);
                this->mModel->AddCell(firstC);
                this->first_cell = 0;
            }
            
			this->ParsePercepts(percept);
		// look test
            if (this->new_cell == 0 && goalHomeMode == 0)
            {
                //std::cout << "looking all directions" << std::endl;
                this->LookDirections();
            }

            //see if i'm at home, if so tell myself so i can recharge
            double diff = 0.1;
			double dx = std::fabs(this->mModel->GetXloc() - 0);
			double dy = std::fabs(this->mModel->GetYloc() - 0);
			if (dx < diff && dy < diff)
            {
                at_home = 0; 
                //std::cout << "Yay I'm at home!" << std::endl;  
            }
            else 
            {
                at_home = 1;
                //std::cout << "on he road again" << std::endl;  
            }
            
            //recharge at base, it'll help you
            if (this->at_home == 0 && this->mModel->GetCharge() < 90.0)
            {
                std::cout << "recharge that thing" << std::endl;
                action->SetCode(ai::Scavenger::Action::RECHARGE);
                return action;
            } 
            
			
			if(this->action_queue.size() == 0 && this->mModel->GetCharge() > 55.0 && !this->game_over && this->in_bag <= 2400)
			{
				this->SearchForGoal();
                this->goalHomeMode = 0;
			}
			if(this->action_queue.size() == 0 && this->mModel->GetCharge() <= 55.0 && !this->game_over && this->in_bag <= 2400)
			{
				this->SearchForHome();
                this->goalHomeMode = 1;
			}
			if(this->action_queue.size() == 0)
			{
                if(!this->game_over)
                {
                    this->SearchForHome();
                }
                if (this->deposit_mode && this->object_store.size() > 0 && !this->got_neg)
                {
                    std::cout << "primatives "<< primative_store[0].size() << " mimics " << mimic_store[0].size() << " junks " << junk_store[0].size() << std::endl << std::flush;
                    if(this->hundred < 100)
                    {
                        
                        action->SetObjectId(this->deposit_queue.back());
                        
                        std::cout << "deposited " << std::endl << std::flush;
                       
                        
                        this->deposit_queue.pop_back();
                        action->SetCode(ai::Scavenger::Action::DEPOSIT);
                        
                        this->hundred++;
                        this->pickup_counter++;
                        return action;
                    }
                    else
                    {
                        double high = -1000.0;
                        std::string object_name = "";
                        std::map<std::string,Object>::iterator it;
			            for(it = this->object_store.begin(); it != this->object_store.end(); it++)
			            {
                            double prob = this->ObjectGood(it->second);
				            if (prob > high)
                            {
                                high = prob;
                                object_name = it->second.GetId();
                            }
			            }
                        std::cout << high << std::endl << std::flush;
                        if(high < 10)
                        {
                            got_neg = true;
                        }
                        action->SetObjectId(object_name);
                        std::cout << "deposited " << std::endl << std::flush;
                        action->SetCode(ai::Scavenger::Action::DEPOSIT);
                        this->pickup_counter++;
                        return action;
                    }
                    std::cout << "primatives " << primative_store[0].size() << " mimics " << mimic_store[0].size() << " junk " << junk_store[0].size() << std::endl << std::flush;
                }
                
                if(game_over)
                {
                    action->SetCode(ai::Scavenger::Action::QUIT);
                }
                game_over = true;
                deposit_mode = true;
				
			}
			else
			{
				Action a = this->action_queue.front();
				this->action_queue.pop_front();
				switch(a.GetType())
				{
					case Action::A_NORTH:
						action->SetCode(ai::Scavenger::Action::GO_NORTH);
                        this->new_cell = 0;
						break;
					case Action::A_SOUTH:
						action->SetCode(ai::Scavenger::Action::GO_SOUTH);
                        this->new_cell = 0;
						break;
					case Action::A_EAST:
						action->SetCode(ai::Scavenger::Action::GO_EAST);
                        this->new_cell = 0;
						break;
					case Action::A_WEST:
						action->SetCode(ai::Scavenger::Action::GO_WEST);
                        this->new_cell = 0;
						break;
					case Action::A_LNORTH:
						action->SetDirection(ai::Scavenger::Location::NORTH);
						action->SetCode(ai::Scavenger::Action::LOOK);
                        this->look_direction = 0;
                        //std::cout << "looking action north" << std::endl;
						break;
					case Action::A_LSOUTH:
						action->SetDirection(ai::Scavenger::Location::SOUTH);
						action->SetCode(ai::Scavenger::Action::LOOK);
                        this->look_direction = 1;
                        //std::cout << "looking action south" << std::endl;
						break;
					case Action::A_LEAST:
						action->SetDirection(ai::Scavenger::Location::EAST);
						action->SetCode(ai::Scavenger::Action::LOOK);
                        this->look_direction = 2;
                        //std::cout << "looking action east" << std::endl;
						break;
					case Action::A_LWEST:
						action->SetDirection(ai::Scavenger::Location::WEST);
						action->SetCode(ai::Scavenger::Action::LOOK);
                        this->look_direction = 3;
                        //std::cout << "looking action west" << std::endl;
						break;
                    case Action::A_RECHARGE:
                        action->SetCode(ai::Scavenger::Action::RECHARGE);
                        break;
                    case Action::A_EXAMINE:
                        action->SetObjectId(this->object_queue.back());
                        this->object_queue.pop_back();
                        action->SetCode(ai::Scavenger::Action::EXAMINE);
                        break;
                    case Action::A_PICKUP:
                        action->SetObjectId(this->pickup_queue.back());
                        this->deposit_queue.push_back(this->pickup_queue.back());
                        this->pickup_queue.pop_back();
                        action->SetCode(ai::Scavenger::Action::PICKUP);
                        this->in_bag++;
                        break;
                    case Action::A_DEPOSIT:
                        std::cout << "probably should never get to this part in this version" << std::endl << std::flush;
                        action->SetObjectId(this->deposit_queue.back());
                        this->deposit_queue.pop_back();
                        action->SetCode(ai::Scavenger::Action::DEPOSIT);
                        this->hundred++;
//                        std::cout << "deposited " << std::endl;
                        break;
					default:
						break;
						action->SetCode(ai::Scavenger::Action::QUIT);
				}
			}
			return action;
		}
	
		bool Bob::ParsePercepts(const ai::Agent::Percept *percept)
		{
			unsigned int i;
			for(i = 0; i < percept->NumAtom(); i++)
			{
				ai::Agent::PerceptAtom a = percept->GetAtom(i);
				//std::cout << a.GetName() << ": " << a.GetValue() << std::endl;
			
				//Start loading percepts into model
				// add Charge to Model
				if(std::strcmp(a.GetName().c_str(), "CHARGE") == 0)
				{
					double charge = std::atof(a.GetValue().c_str());
					this->mModel->SetCharge(charge);
				}
				// add HP to Model
				else if(std::strcmp(a.GetName().c_str(), "HP") == 0)
				{
					double hp = std::atof(a.GetValue().c_str());
					this->mModel->SetHP(hp);
				}
				// add X_LOC to model
				else if(std::strcmp(a.GetName().c_str(), "X_LOC") == 0)
				{
					double x_loc = std::atof(a.GetValue().c_str());
					this->mModel->SetXloc(x_loc);
				}
				// add Y_LOC to model
				else if(std::strcmp(a.GetName().c_str(), "Y_LOC") == 0)
				{
					double y_loc = std::atof(a.GetValue().c_str());
					this->mModel->SetYloc(y_loc);
				}
				// add Z_LOC to model
				else if(std::strcmp(a.GetName().c_str(), "Z_LOC") == 0)
				{
					double z_loc = std::atof(a.GetValue().c_str());
					this->mModel->SetZloc(z_loc);
				}
                // object parsing
                
                else if((std::strncmp(a.GetName().c_str(), "OBJECT_", 7) == 0)&&(this->mModel->GetCellVisited(this->mModel->GetXloc(),this->mModel->GetYloc())!=true) && goalHomeMode == 0)
                {
                        // pick up everything!
                        char objectName[16];
                        std::sscanf(a.GetValue().c_str(), "%[^,]", objectName);
                        //std::cout << objectName << std::endl;
                        this->object_queue.push_back(objectName);
                        
                        Action * objEx = new Action();
                        objEx->SetType(Action::A_EXAMINE);
                        this->action_queue.push_back(*objEx);
                        Action * pickEx = new Action();
                        pickEx->SetType(Action::A_PICKUP);
                        this->action_queue.push_back(*pickEx);
                }
                // examine parsing
                else if(std::strcmp(a.GetName().c_str(), "EXAMINE")== 0)
                {
                    char object[16], color[16], shape[16], size[16];
                    std::sscanf(a.GetValue().c_str(), "%[^ ] %[^ ] %[^ ] %[^ ]", object, color,shape,size);
                    std::cout << object << " 1 " << color << " 2 " << shape << " 3 " << size << std::endl;
                    // stuff everything into my objects que and examine stuff
                    
                    
                    // create an object and stuff into the list of objects, will fill out points for it later
                    Object newObject = Object(size,shape,color,object,0);
                    this->pickup_queue.push_back(object);
//                  Action * objPick = new Action();
//                        objPick->SetType(Action::A_PICKUP);
//                        this->action_queue.push_back(*objPick);
                        
                    
                    
                    this->object_store[object] = newObject;
                }
                else if(std::strcmp(a.GetName().c_str(), "DEPOSIT_VALUE")== 0)
                {
                    //deposit value parsing
                    char object[16], points[16];
                    std::sscanf(a.GetValue().c_str(), "%[^ ] %[^ ]", object, points);
                    int intPoints = std::atoi(points);
                    this->object_store[object].SetPoints(intPoints);

                    Object newObject = object_store[object];
            
                    if (intPoints == 50)
                    {
                        //if primative, get from object store and store in primative store
                        primative_store[0].push_back(newObject.GetShape());
                        primative_store[1].push_back(newObject.GetSize());
                        primative_store[2].push_back(newObject.GetColor());
                    }
                    else if (intPoints == 10)
                    {
                        mimic_store[0].push_back(newObject.GetShape());
                        mimic_store[1].push_back(newObject.GetSize());
                        mimic_store[2].push_back(newObject.GetColor());
                    }
                    else if (intPoints == -50)
                    {
                        junk_store[0].push_back(newObject.GetShape());
                        junk_store[1].push_back(newObject.GetSize());
                        junk_store[2].push_back(newObject.GetColor());
//                        if(this->hundred >= 100)
//                        {
//                            this->got_neg++;
//                        }
                    }
                    else
                    {
                        std::cout << "got a weird point value in DEPOSIT_VALUE in ParsePercepts" << std::endl;
                    }
                    std::map<std::string,Object>::iterator it;
                    it=this->object_store.find(newObject.GetId());
                    this->object_store.erase (it);
                }
                // looks
				else if(std::strcmp(a.GetName().c_str(), "LOOK") == 0)
				{
					char look_terrain[16];
					std::sscanf(a.GetValue().c_str(), "%[^,]", look_terrain);
					int terrain_num;
					terrain_num = Cell::TerrainLabelToEnum(look_terrain);
                    int dx[4] = {0,0,1,-1};
			        int dy[4] = {1,-1,0,0};
                    double newCellX = this->mModel->GetXloc() + 1000.*dx[look_direction];
				    double newCellY = this->mModel->GetYloc() + 1000.*dy[look_direction];
                    double newCellZ = this->mModel->GetZloc();
					if(look_direction == 0)
					{
                        Cell newCell(newCellX,newCellY,newCellZ,I_ND,terrain_num,I_ND,I_ND);
                        this->mModel->SetCellTerrain(this->mModel->GetXloc(),
                        this->mModel->GetYloc(),look_direction,terrain_num);
                        if (terrain_num != I_WALL)
                        {
                            if (!this->mModel->AddCell(newCell))
						    {
                                if (terrain_num == I_CLIFF)
                                {
                                    this->mModel->SetCellTerrain(newCellX,newCellY,1,I_WALL);
                                }
                                else
                                {
                                    this->mModel->SetCellTerrain(newCellX,newCellY,1,terrain_num);
                                }
                            }
                        }
					}
                    if(look_direction == 1)
					{
                        Cell newCell(newCellX,newCellY,newCellZ,terrain_num,I_ND,I_ND,I_ND);
                        this->mModel->SetCellTerrain(this->mModel->GetXloc(),
                        this->mModel->GetYloc(),look_direction,terrain_num);
                        if (terrain_num != I_WALL)
                        {
                            if (!this->mModel->AddCell(newCell))
						    {
                                if (terrain_num == I_CLIFF)
                                {
                                    this->mModel->SetCellTerrain(newCellX,newCellY,0,I_WALL);
                                }
                                else
                                {
                                    this->mModel->SetCellTerrain(newCellX,newCellY,0,terrain_num);
                                }
                            }
                        }
					}
                    if(look_direction == 2)
					{
                        Cell newCell(newCellX,newCellY,newCellZ,I_ND,I_ND,I_ND,terrain_num);
                        this->mModel->SetCellTerrain(this->mModel->GetXloc(),
                        this->mModel->GetYloc(),look_direction,terrain_num);
                        if (terrain_num != I_WALL)
                        {
                            if (!this->mModel->AddCell(newCell))
						    {
                                if (terrain_num == I_CLIFF)
                                {
                                    this->mModel->SetCellTerrain(newCellX,newCellY,3,I_WALL);
                                }
                                else
                                {
                                    this->mModel->SetCellTerrain(newCellX,newCellY,3,terrain_num);
                                }
                            }
                        }
					}
                    if(look_direction == 3)
					{
                        Cell newCell(newCellX,newCellY,newCellZ,I_ND,I_ND,terrain_num,I_ND);
                        this->mModel->SetCellTerrain(this->mModel->GetXloc(),
                        this->mModel->GetYloc(),look_direction,terrain_num);
                        if (terrain_num != I_WALL)
                        {
                            if (!this->mModel->AddCell(newCell))
						    {
                                if (terrain_num == I_CLIFF)
                                {
                                    this->mModel->SetCellTerrain(newCellX,newCellY,2,I_WALL);
                                }
                                else
                                {
                                    this->mModel->SetCellTerrain(newCellX,newCellY,2,terrain_num);
                                }
                            }
                        }
					}
				}
			}
			return true;
		}
	
	
		bool Bob::SearchForGoal()
		{
			bool done = false;
			int depth = 1;
			if(this->fringe_type == FRINGE_DLS)
			{
				depth = this->max_depth;
			}
		
			while(!done)
			{
				State *init_state = new State();
				init_state->SetX(this->mModel->GetXloc());
				init_state->SetY(this->mModel->GetYloc());
				init_state->SetZ(this->mModel->GetZloc());
				init_state->SetCharge(this->mModel->GetCharge());
				init_state->SetHP(this->mModel->GetHP());
			
				//State *goal_state = new State();
				//goal_state->SetX(this->mModel->GetGX());
				//goal_state->SetY(this->mModel->GetGY());
				//goal_state->SetZ(this->mModel->GetGZ());
			
				Problem *the_problem = new Problem(init_state, this->mModel);
				//the_problem->SetGoal(goal_state);
			
				ai::Search::Fringe *the_frontier = 0;
				switch(this->fringe_type)
				{
					case FRINGE_BFS:
						the_frontier = new ai::Search::BFFringe();
						done = true;
						break;
					case FRINGE_UCS:
						the_frontier = new ai::Search::UCFringe();
						done = true;
						break;
					case FRINGE_DFS:
						the_frontier = new ai::Search::DFFringe();
						done = true;
						break;
					case FRINGE_DLS:
						the_frontier = new ai::Search::DLFringe(depth);
						done = true;
						break;
					case FRINGE_IDS:
						the_frontier = new ai::Search::DLFringe(depth);
						if(depth == this->max_depth)
						{
							done = true;
						}
						depth++;
						break;
				}
				ai::Search::Algorithm *the_algorithm = 0;
				switch(this->alg_type)
				{
					case ALG_TREE:
						the_algorithm = new ai::Search::Tree(the_problem, the_frontier);
						break;
					
					case ALG_GRAPH:
						the_algorithm = new ai::Search::Graph(the_problem, the_frontier);
						break;
				}

				if(the_algorithm->Search())
				{
					std::list<ai::Search::Node *> *solution = the_algorithm->GetSolution().GetList();
					std::list<ai::Search::Node *>::const_iterator it;
				
					double cost = 0;
					int depth = 0;
				
					for(it = solution->begin(); it != solution->end(); it++)
					{
						if((*it)->GetAction())
						{
							const Action * const action = dynamic_cast<const Action * const>((*it)->GetAction());
							std::cout << *action << " ";
							this->action_queue.push_back(*action);
						}
					
						if((*it)->GetState())
						{
							const State * const state = dynamic_cast<const State * const>((*it)->GetState());
							std::cout << *state << " ";
						}
						cost = (*it)->GetPathCost();
						depth = (*it)->GetDepth();
						std::cout << std::endl;
					}
					size_t nodes_generated = the_algorithm->GetNumberNodesGenerated();
					size_t nodes_stored = the_algorithm->GetMaxNodesStored();
					std::cout << "Results: T " << cost << " " << depth << " "
							  << nodes_generated << " " << nodes_stored << std::endl;
					done = true;
				}
				else
				{
					size_t nodes_generated = the_algorithm->GetNumberNodesGenerated();
					size_t nodes_stored = the_algorithm->GetMaxNodesStored();
					double cost = -1;
					int depth = -1;
					std::cout << "Results: F " << cost << " " << depth << " "
							  << nodes_generated << " " << nodes_stored << std::endl;
				}
				delete the_algorithm;
			}
			return true;
		}
		bool Bob::SearchForHome()
		{
			bool done = false;
			int depth = 1;
			if(this->fringe_type == FRINGE_DLS)
			{
				depth = this->max_depth;
			}
		
			while(!done)
			{
				State *init_state = new State();
				init_state->SetX(this->mModel->GetXloc());
				init_state->SetY(this->mModel->GetYloc());
				init_state->SetZ(this->mModel->GetZloc());
				init_state->SetCharge(this->mModel->GetCharge());
				init_state->SetHP(this->mModel->GetHP());
			
				State *goal_state = new State();
				goal_state->SetX(0.0000000);
				goal_state->SetY(0.0000000);
				goal_state->SetZ(0.0000000);
			
				Problem *the_problem = new Problem(init_state, this->mModel,true);
				the_problem->SetGoal(goal_state);
			    
                //set my go home to A*-Graph search
				ai::Search::Fringe *the_frontier = 0;

				the_frontier = new ai::Search::AStarFringe();
				done = true;

				ai::Search::Algorithm *the_algorithm = 0;

				the_algorithm = new ai::Search::Graph(the_problem, the_frontier);
					

				if(the_algorithm->Search())
				{
					std::list<ai::Search::Node *> *solution = the_algorithm->GetSolution().GetList();
					std::list<ai::Search::Node *>::const_iterator it;
				
					double cost = 0;
					int depth = 0;
				
					for(it = solution->begin(); it != solution->end(); it++)
					{
						if((*it)->GetAction())
						{
							const Action * const action = dynamic_cast<const Action * const>((*it)->GetAction());
							std::cout << *action << " ";
							this->action_queue.push_back(*action);
						}
					
						if((*it)->GetState())
						{
							const State * const state = dynamic_cast<const State * const>((*it)->GetState());
							std::cout << *state << " ";
						}
						cost = (*it)->GetPathCost();
						depth = (*it)->GetDepth();
						std::cout << std::endl;
					}
					size_t nodes_generated = the_algorithm->GetNumberNodesGenerated();
					size_t nodes_stored = the_algorithm->GetMaxNodesStored();
					std::cout << "Results: T " << cost << " " << depth << " "
							  << nodes_generated << " " << nodes_stored << std::endl;
                    //when at home, recharge
                    
//                    Action * a = new Action();
//                    a->SetType(Action::A_RECHARGE);
//		            this->action_queue.push_back(*a);
//                    this->action_queue.push_back(*a);
//                    this->action_queue.push_back(*a);
                
					done = true;
                   
                    
                    
				}
				else
				{
					size_t nodes_generated = the_algorithm->GetNumberNodesGenerated();
					size_t nodes_stored = the_algorithm->GetMaxNodesStored();
					double cost = -1;
					int depth = -1;
					std::cout << "Results: F " << cost << " " << depth << " "
							  << nodes_generated << " " << nodes_stored << std::endl;
				}
				delete the_algorithm;
			}
			return true;
		}
        // look in all 4 directions if terrain not discovered
		bool Bob::LookDirections()
		{
			Action * a = new Action();
			double x = this->mModel->GetXloc();
            double y = this->mModel->GetYloc();

            this->mModel->SetCellVisited(x,y,true);
            this->new_cell = 1;
			for (int direction = 0; direction < 4; direction++)
			{
				if (direction == 0)
				{
                    //std::cout << "looking north" << std::endl;
                    
                    if(this->mModel->GetCellTerrain(x,y,direction) == I_ND)
                    {
                        //std::cout << "north terrain" << std::endl;
                       
					    a->SetType(Action::A_LNORTH);
					    this->action_queue.push_back(*a);
                    }
				}
				else if (direction == 1)
				{
                    //std::cout << "looking south" << std::endl;
                    if(this->mModel->GetCellTerrain(x,y,direction) == I_ND)
                    {
                        //std::cout << "south terrain" << std::endl;
					    a->SetType(Action::A_LSOUTH);
					    this->action_queue.push_back(*a);
                    }
				}
				else if (direction == 2)
				{
                    //std::cout << "looking east" << std::endl;
                    if(this->mModel->GetCellTerrain(x,y,direction) == I_ND)
                    {
                        //std::cout << "east terrain" << std::endl;
					    a->SetType(Action::A_LEAST);
					    this->action_queue.push_back(*a);
                    }
				}
				else if (direction == 3)
				{
                    //std::cout << "looking west" << std::endl;
                    if(this->mModel->GetCellTerrain(x,y,direction) == I_ND)
                    {
                        //std::cout << "west terrain" << std::endl;
					    a->SetType(Action::A_LWEST);
					    this->action_queue.push_back(*a);
                    }
				}
			}
			return true;
		}
        // if object queue not empty examine objects
        double Bob::ObjectGood(Object object)
        {
//           std::cout << "hello from ObjectGood" << std::endl << std::flush;
//            std::cout << object.GetId() << " " << object.GetSize() << " " << object.GetShape() << " " << object.GetColor() << std::endl;
            double totalObjects = this->pickup_counter;
            double primativeShape = 0;
            double primativeSize = 0;
            double primativeColor = 0;
            double mimicShape = 0;
            double mimicSize = 0;
            double mimicColor = 0;
            double junkShape = 0;
            double junkSize = 0;
            double junkColor = 0;



            for (std::vector<Object>::size_type iter = 0; iter != primative_store[0].size(); iter++)
            {
                if (primative_store[0][iter] == object.GetShape())
                {
                    primativeShape++;
                }  
                if (primative_store[1][iter] == object.GetSize())
                {
                    primativeSize++;
                }  
                if (primative_store[2][iter] == object.GetColor())
                {
                    primativeColor++;
                }                
            }
            for (std::vector<Object>::size_type iter = 0; iter != mimic_store[0].size(); iter++)
            {
                if (mimic_store[0][iter] == object.GetShape())
                {
                    mimicShape++;
                }  
                if (mimic_store[1][iter] == object.GetSize())
                {
                    mimicSize++;
                }  
                if (mimic_store[2][iter] == object.GetColor())
                {
                    mimicColor++;
                }                
            }
            for (std::vector<Object>::size_type iter = 0; iter != junk_store[0].size(); iter++)
            {
                //std::cout << "shape "<< junk_store[0][iter] << " size " << junk_store[1][iter] << " color " << junk_store[2][iter] << std::endl << std::flush;
                if (junk_store[0][iter] == object.GetShape())
                {
                    junkShape++;
                }  
                if (junk_store[1][iter] == object.GetSize())
                {
                    junkSize++;
                }  
                if (junk_store[2][iter] == object.GetColor())
                {
                   junkColor++;
                }                
            }
//            std::cout << "junkshape matches " << junkShape << std::endl << std::flush;
//           std::cout << "junksize matches " << junkSize << std::endl << std::flush;
//            std::cout << "junkcolor matches " << junkColor << std::endl << std::flush;
            //a whole bunch of calculations, if i have problems look here first
            // P(Primitive) = primitive/total
            double pPrim = primative_store[0].size()/totalObjects;
            double notPrim = (totalObjects - primative_store[0].size())/totalObjects;
            double pMim = mimic_store[0].size()/totalObjects;
            double notMim = (totalObjects - mimic_store[0].size())/totalObjects;
            double pJunk = junk_store[0].size()/totalObjects;
            double notJunk = (totalObjects - junk_store[0].size())/totalObjects;
//            std::cout << "prob junk "  << pJunk << " prob not junk " << notJunk <<std::endl << std::flush;
            ///////////////primitive calculations//////////////////////////////////////
            // P(Shape|Primitive) = probPrimShape
            double pShapePrim = (primativeShape/primative_store[0].size());
            double pShapeNotPrim =((mimicShape+junkShape)/(totalObjects-primative_store[0].size()));
            double pSizePrim = (primativeSize/primative_store[1].size());
            double pSizeNotPrim =((mimicSize+junkSize)/(totalObjects-primative_store[1].size()));
            double pColorPrim = (primativeColor/primative_store[2].size());
            double pColorNotPrim =((mimicColor+junkColor)/(totalObjects-primative_store[2].size()));
            // P(Shape) = P(Shape|Primitive)*P(Primitive) + P(Shape|!Primitive)*P(!Primitive)
            double ppShape = (pShapePrim * pPrim) + (pShapeNotPrim*notPrim);
            double ppSize = (pSizePrim * pPrim) + (pSizeNotPrim*notPrim);
            double ppColor = (pColorPrim * pPrim) + (pColorNotPrim*notPrim);
            // P(Primitive|Shape) = (P(Shape|Primitive)*P(Primitive))/P(Shape)
            double pPrimShape = (pShapePrim*pPrim)/ppShape;
            double pPrimSize = (pSizePrim*pPrim)/ppSize;
            double pPrimColor = (pColorPrim*pPrim)/ppColor;
            ///////////////mimic calculations//////////////////////////////////////
            // P(Shape|Primitive) = probPrimShape
            double pShapeMim = (mimicShape/mimic_store[0].size());
            double pShapeNotMim =((primativeShape+junkShape)/(totalObjects-mimic_store[0].size()));
            double pSizeMim = (mimicSize/mimic_store[1].size());
            double pSizeNotMim =((primativeSize+junkSize)/(totalObjects-mimic_store[1].size()));
            double pColorMim = (mimicColor/mimic_store[2].size());
            double pColorNotMim =((primativeColor+junkColor)/(totalObjects-mimic_store[2].size()));
            // P(Shape) = P(Shape|Primitive)*P(Primitive) + P(Shape|!Primitive)*P(!Primitive)
            double pmShape = (pShapeMim * pMim) + (pShapeNotMim*notMim);
            double pmSize = (pSizeMim * pMim) + (pSizeNotMim*notMim);
            double pmColor = (pColorMim * pMim) + (pColorNotMim*notMim);
            // P(Primitive|Shape) = (P(Shape|Primitive)*P(Primitive))/P(Shape)
            double pMimShape = (pShapeMim*pMim)/pmShape;
            double pMimSize = (pSizeMim*pMim)/pmSize;
            double pMimColor = (pColorMim*pMim)/pmColor;
            ///////////////junk calculations//////////////////////////////////////
            // P(Shape|Primitive) = probPrimShape
            double pShapeJunk = (junkShape/junk_store[0].size());
            double pShapeNotJunk =((primativeShape+mimicShape)/(totalObjects-junk_store[0].size()));
            //std::cout << "prob shape|junk "  << pShapeJunk << " prob shape|!junk" << pShapeNotJunk <<std::endl << std::flush;
            double pSizeJunk = (junkSize/junk_store[1].size());
            double pSizeNotJunk =((primativeSize+mimicSize)/(totalObjects-junk_store[1].size()));
            //std::cout << "prob size|junk "  << pSizeJunk << " prob size|!junk" << pSizeNotJunk <<std::endl << std::flush;
            double pColorJunk = (junkColor/junk_store[2].size());
            double pColorNotJunk =((primativeColor+mimicColor)/(totalObjects-junk_store[2].size()));
            //std::cout << "prob color|junk "  << pColorJunk << " prob color|!junk" << pColorNotJunk <<std::endl << std::flush;
            // P(Shape) = P(Shape|Primitive)*P(Primitive) + P(Shape|!Primitive)*P(!Primitive)
            double pjShape = (pShapeJunk * pJunk) + (pShapeNotJunk*notJunk);
            //std::cout << "prob shape "  << pjShape <<std::endl << std::flush;
            double pjSize = (pSizeJunk * pJunk) + (pSizeNotJunk*notJunk);
            //std::cout << "prob size "  << pjSize <<std::endl << std::flush;
            double pjColor = (pColorJunk * pJunk) + (pColorNotJunk*notJunk);
            //std::cout << "prob color "  << pjColor <<std::endl << std::flush;
            // P(Primitive|Shape) = (P(Shape|Primitive)*P(Primitive))/P(Shape)
            double pJunkShape = (pShapeJunk*pJunk)/pjShape;
            //std::cout << "prob junk|shape "  << pJunkShape <<std::endl << std::flush;
            double pJunkSize = (pSizeJunk*pJunk)/pjSize;
            //std::cout << "prob junk|size "  << pJunkSize <<std::endl << std::flush;
            double pJunkColor = (pColorJunk*pJunk)/pjColor;
            //std::cout << "prob junk|color "  << pJunkColor <<std::endl << std::flush;
            
//            std::cout << "probability is Primative: " << (pPrimShape+pPrimSize+pPrimColor)/3 << std::endl << std::flush;
//            std::cout << "probability is Mimic: " << (pMimShape+pMimSize+pMimColor)/3 << std::endl << std::flush;
//            std::cout << "probability is Junk: " << (pJunkShape+pJunkSize+pJunkColor)/3 << std::endl << std::flush;
            double primativeScore = 50.0;
            double mimicScore = 10.0;
            double junkScore = -50.0;
            return (primativeScore*((pPrimShape+pPrimSize+pPrimColor)/3))+(mimicScore*((pMimShape+pMimSize+pMimColor)/3))+(junkScore*((pJunkShape+pJunkSize+pJunkColor)/3));        
//            return 1-((pJunkShape+pJunkSize+pJunkColor)/3);
        }
    }
}
