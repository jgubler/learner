#ifndef _JAGBOB_H_
#define _JAGBOB_H_

#include <ai_scavenger.h>
#include <list>
#include <vector>
#include "jagModel3.h"
#include "jagAction3.h"


namespace jag
{
	namespace bb
	{
		enum
		{
			ALG_MIN,
			ALG_TREE,
			ALG_GRAPH,
			ALG_MAX
		};
		enum
		{
			FRINGE_MIN,
			FRINGE_BFS,
			FRINGE_UCS,
			FRINGE_DFS,
			FRINGE_DLS,
			FRINGE_IDS,
			FRINGE_MAX
		};
	    class Bob : public ai::Agent::AgentProgram
	    {
	    public:
	      Bob(ai::Agent::Options *opts);
	      ~Bob();
	      virtual ai::Agent::Action * Program(const ai::Agent::Percept * percept);
	    protected:
			bool ParsePercepts(const ai::Agent::Percept *percept);
			bool SearchForGoal();
			bool SearchForHome();
			bool LookDirections();
            double ObjectGood(Object object);
		
			Model *mModel;
			int alg_type;
			int fringe_type;
			int max_depth;
			int goalHomeMode;
			int look_direction;
            int new_cell;
            int first_cell;
            int at_home;
            bool game_over;
            int parse_hundred;
            
            
            int hundred;
            bool deposit_mode;
            bool got_neg;
			int pickup_counter;
            int in_bag;

		    std::vector<std::string> object_queue;
            std::vector<std::string> pickup_queue;
            std::vector<std::string> deposit_queue;
            //object store
            std::map<std::string,Object> object_store;
            /////learning////////////vectors
            //vector spot 0 will be shape, spot 1 size, spot 2 color
            std::vector<std::vector<std::string> > primative_store;
            std::vector<std::vector<std::string> > mimic_store;
            std::vector<std::vector<std::string> > junk_store;
            

			std::list<Action> action_queue;
	    private:
	    };
	}
}

#endif /* _jagBob_H_ */
