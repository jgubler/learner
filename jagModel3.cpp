#include "jagModel3.h"
#include <cstdio>
#include <cstring>
#include <map>
#include <utility>
#include <cmath>

//James Gubler
//Model for ScavengerRobot ai
//8-29-2013

namespace jag
{
	namespace bb
	{
		static const char *terrain_labels[] = 
		{
			"cliff", "mud", "plain", "rocks", "wall", "ice", "nd","error"
		};
		//Cell Class//
		Cell::Cell()
		{
			*this = Cell::None();
		}
	
		Cell::Cell(const Cell &rhs)
		{
			*this = rhs;
		}
	
		Cell::Cell(const double &x, const double &y, const double &z, 
				 const int &north, const int &south, const int &east, 
				 const int &west, const int &visited)
			:mX(x),mY(y),mZ(z),mVisited(visited)
		{
			this->terrains[0] = north;
			this->terrains[1] = south;
			this->terrains[2] = east;
			this->terrains[3] = west;
		}

		Cell::~Cell()
		{
		}
	
		Cell &Cell::operator=(const Cell &rhs)
		{
			this->mX = rhs.mX;
			this->mY = rhs.mY;
			this->mZ = rhs.mZ;
			for(int dir = 1 + (int)D_MIN; dir < (int) D_MAX; dir++)
			{
				this->terrains[dir] = rhs.terrains[dir];
			}
			this->mVisited = rhs.mVisited;
			return *this;
		}

		void Cell::SetVisited(const bool visited)
		{
			this->mVisited = visited;
		}
		int Cell::SetTerrain(const int &direction, const int &terrain)
		{
			if(direction <= D_MIN || direction >= D_MAX)
			{
				return I_OUT;
			}
			this->terrains[direction] = terrain;
            return 1;
		}
	
		bool Cell::operator==(const Cell &rhs) const
		{
			double diff = 0.1;
			double dx = std::fabs(this->mX - rhs.mX);
			double dy = std::fabs(this->mY - rhs.mY);
			double dz = std::fabs(this->mZ - rhs.mZ);
		
			bool tAll = true;
			for(int ter = 1 + (int)D_MIN; ter < (int)D_MAX; ter++)
			{
				tAll &= (this->terrains[ter] == rhs.terrains[ter]);
			}
			
			return (dx < diff && dy < diff && dz < diff && tAll && this->mVisited == rhs.mVisited);
		}
	
		double Cell::GetX() const
		{
			return this->mX;
		}
	
		double Cell::GetY() const
		{
			return this->mY;
		}
	
		double Cell::GetZ() const
		{
			return this->mZ;
		}
	
		int Cell::GetTerrain(const int &direction) const
		{
			if(direction <= D_MIN || direction >= D_MAX)
			{
				return I_OUT;
			}
			return this->terrains[direction];
		}

		bool Cell::GetVisited() const
		{
			return this->mVisited;
		}
	
		const Cell Cell::None()
		{
			return Cell (0.,0.,0.,I_OUT,I_OUT,I_OUT,I_OUT);
		}
	
		int Cell::TerrainLabelToEnum(const char *s)
		{
			for(int ter = 1 + (int)I_MIN; ter < (int) I_MAX; ter++)
			{
				if(std::strcmp(s,terrain_labels[ter]) == 0)
				{
					return ter;
				}
			}
			return I_OUT;
		}
	
		const char *Cell::TerrainEnumToLabel(const int &num)
		{
			if(num <= (int)I_MIN || num >= (int)I_MAX)
			{
				return terrain_labels[I_MAX];
			}
			return terrain_labels[num];
		}
		
		bool Cell::Display(std::ostream &os) const
		{
			os << this->mX << ","
			   << this->mY << ","
			   << this->mZ << " ";
			   
			for(int ter = 1 + (int)D_MIN; ter<(int)D_MAX; ter++)
			{
				if(ter>1 + (int)D_MIN)
				{
					os << ",";
				}
				os << Cell::TerrainEnumToLabel(this->terrains[ter]);
			}
			os << this->mVisited;
			return true;
		}
	
		std::ostream &operator<<(std::ostream &os, const Cell &rhs)
		{
			rhs.Display(os);
			return os;
		}
////////////////////////////////object Class///////////////////////////////////////////////
        Object::Object()
            :mSize(""),mShape(""),mColor(""),mId(""),mPoints(0)
        {
        }
        Object::Object(std::string size, std::string shape, std::string color, std::string id, int points)
            :mSize(size),mShape(shape),mColor(color),mId(id),mPoints(points)
        {
        }
        Object::~Object()
        {
        }
    
        std::string Object::GetSize()
        {
            return this->mSize;
        }
        std::string Object::GetShape()
        {
            return this->mShape;
        }
        std::string Object::GetColor()
        {
            return this->mColor;
        }
        std::string Object::GetId()
        {
            return this->mId;
        }
        int Object::GetPoints()
        {
            return this->mPoints;
        }
        
        void Object::SetSize(std::string size)
        {
            this->mSize = size;
        }
        void Object::SetShape(std::string shape)
        {
            this->mShape = shape;
        }
        void Object::SetColor(std::string color)
        {
            this->mColor = color;
        }
        void Object::SetId(std::string id)
        {
            this->mId = id;
        }
        void Object::SetPoints(int points)
        {
            this->mPoints = points;
        }

////////////////////////////////////////Model Class/////////////////////////////////////////
	
		Model::Model()
		: mXLoc(0.),mYLoc(0.),mZLoc(0.),mGX(0.),
		mGY(0.),mGZ(0.),mCharge(0.),mHP(0.)
		{
		}
	
		Model::~Model()
		{
		}
	
		double Model::GetCharge() const
		{
			return this->mCharge;
		}
		double Model::GetHP() const
		{
			return this->mHP;
		}
		double Model::GetXloc() const
		{
			return this->mXLoc;
		}
		double Model::GetYloc() const
		{
			return this->mYLoc;
		}
		double Model::GetZloc() const
		{
			return this->mZLoc;
		}
		double Model::GetGX() const
		{
			return this->mGX;
		}
		double Model::GetGY() const
		{
			return this->mGY;
		}
		double Model::GetGZ() const
		{
			return this->mGZ;
		}
           
        int Model::GetCellTerrain(const double &x, const double &y, const int &direction) 
        {
            std::pair <double,double> key;
			key = std::make_pair(x,y);
            return this->mCells[key].GetTerrain(direction);
        }

        bool Model::GetCellVisited(const double &x, const double &y) 
        {
            std::pair <double,double> key;
			key = std::make_pair(x,y);
            Cell c =this->mCells[key];
            return c.GetVisited();
        }

	
		bool Model::AddCell(const Cell &c)
		{
			double x = c.GetX();
			double y = c.GetY();
			std::pair <double,double> key;
			key = std::make_pair(x,y);
			std::map<std::pair<double,double>,Cell>::const_iterator it;
			it = this->mCells.find(key);
			if(it != this->mCells.end())
			{
				return false;
			}
			this->mCells[key] = c;
			return true;
		}

		Cell Model::GetCell(const double &x, const double &y)
		{
			std::pair <double,double> key;
			key = std::make_pair(x,y);
		
			std::map<std::pair<double,double>,Cell>::const_iterator it;
			it = this->mCells.find(key);
			if(it == this->mCells.end())
			{
				return Cell::None();
			}
			return it->second;
		}
	
		void Model::SetCharge(const double &charge)
		{
			this->mCharge = charge;
		}
		void Model::SetHP(double &hp)
		{
			this->mHP = hp;
		}
		void Model::SetXloc(const double &x)
		{
			this->mXLoc = x;
		}
		void Model::SetYloc(const double &y)
		{
			this->mYLoc = y;
		}
		void Model::SetZloc(const double &z)
		{
			this->mZLoc = z;
		}
		void Model::SetGoalLoc(const double &gx, const double &gy, const double &gz)
		{
			this->mGX = gx;
			this->mGY = gy;
			this->mGZ = gz;
		}

        void Model::SetCellTerrain(const double &x, const double &y, const int &direction, const int &terrain)
        {
            std::pair <double,double> key;
			key = std::make_pair(x,y);
            this->mCells[key].SetTerrain(direction,terrain);
        }
        
        void Model::SetCellVisited(const double &x, const double &y,const bool &visited)
        {
            std::pair <double,double> key;
			key = std::make_pair(x,y);
            this->mCells[key].SetVisited(visited);
        }
		
		bool Model::Display(std::ostream &os) const
		{
			os << "Pos: "
			   << this->mXLoc << ","
			   << this->mYLoc << ","
			   << this->mZLoc << std::endl;
			   
			os << "Charge: "
			   << this->mCharge
			   << " HP: " << this->mHP << std::endl;
			   
			os << "Goal: "
			   << this->mGX << ","
			   << this->mGY << ","
			   << this->mGZ << std::endl;
			   
			std::map<std::pair<double,double>,Cell>::const_iterator it;
			for(it = this-> mCells.begin(); it != this->mCells.end(); it++)
			{
				os << it->second << std::endl;
			}
			return true;
		}
	
		std::ostream &operator<<(std::ostream &os, const Model &rhs)
		{
			rhs.Display(os);
			return os;
		}
	}
}
