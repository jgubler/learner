#ifndef _JAGACTION3_H_
#define _JAGACTION3_H_

#include <ai_search.h>

namespace jag
{
	namespace bb
	{
		class Action : public ai::Search::Action
		{
		public:
			enum
				{
					A_MIN = -1,
					A_NORTH = 0,
					A_SOUTH = 1,
					A_EAST = 2,
					A_WEST = 3,
					A_LNORTH = 4,
					A_LSOUTH = 5,
					A_LEAST = 6,
					A_LWEST = 7,
                    A_RECHARGE = 8,
                    A_EXAMINE = 9,
                    A_PICKUP = 10,
                    A_DEPOSIT = 11,
                    A_DROP = 12,
					A_MAX = 13,
					A_OUT
				};

			Action();
			Action(const Action &rhs);
			Action &operator=(const Action &rhs);
			bool operator==(const Action &rhs) const;
			virtual ~Action();
			virtual void Display() const;

			void SetType(int newType);
			int GetType() const;
		
			static int LabelToEnum(const char *s);
			static const char *EnumToLabel(const int &i);
		
			void Display(std::ostream &os) const;

		protected:
			int mType;
		private:
		};
		std::ostream &operator<<(std::ostream &os, const Action &rhs);
	}
}

#endif /* _jagAction3_h_ */
