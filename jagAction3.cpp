#include <iostream>
#include <cstring>
#include "jagAction3.h"

namespace jag
{
	namespace bb
	{
		static const char *actionLabels[] = 
		{
			"go_north", "go_south", "go_east", "go_west", "look_north", "look_south", "look_east", "look_west", "error"
		};
		Action::Action(): mType(A_MIN)
		{
		}

		Action::Action(const Action &rhs)
		{
			*this = rhs;
		}
	
		Action::~Action()
		{
		}

		Action &Action::operator=(const Action &rhs)
		{
			this->mType = rhs.mType;
			return *this;
		}

		bool Action::operator==(const Action &rhs) const
		{
			return(this->mType == rhs.mType);
		}

		void Action::Display() const
		{
			this->Display(std::cout);
			std::cout << std::endl;
		}

		void Action::SetType(int newType)
		{
				this->mType = newType;
		}
		
		int Action::GetType() const
		{
			return this->mType;
		}
	
		int Action::LabelToEnum(const char *s)
		{
			for(int label = 1+(int) A_MIN; label < (int) A_MAX; label++)
			{
				if(std::strcmp(s,actionLabels[label]) == 0)
				{
					return label;
				}
			}
			return A_OUT;
		}
	
		const char *Action::EnumToLabel(const int &num)
		{
			if(num <= (int)A_MIN || num >= (int)A_MAX)
			{
				return actionLabels[A_MAX];
			}
			return actionLabels[num];
		}
	
		void Action::Display(std::ostream &os) const
		{
			os << Action::EnumToLabel(this->mType);
		}
	
		std::ostream &operator<<(std::ostream &os, const Action &rhs)
		{
			rhs.Display(os);
			return os;
		}
	}
}
