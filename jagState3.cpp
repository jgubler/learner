#include <iostream>
#include <cmath>
#include "jagState3.h"


namespace jag
{
	namespace bb
	{
		State::State()
		{
		}
		State::State(const State &rhs)
		{
			*this = rhs;
		}
		State::~State()
		{
		}
		
		State &State::operator=(const State &rhs)
		{
			this->mX = rhs.mX;
			this->mY = rhs.mY;
			this->mZ = rhs.mZ;
			this->mCharge = rhs.mCharge;
			this->mHP = rhs.mHP;
			return *this;
		}
		bool State::operator==(const State &rhs) const
		{
			double diff = 0.1;
			double dx = std::fabs(this->mX - rhs.mX);
			double dy = std::fabs(this->mY - rhs.mY);
			return (dx < diff && dy < diff);
			//never == two floats, it doesn't end well
		}

		void State::Display() const
		{
			this->Display(std::cout);
			std::cout << std::endl;
		}

		bool State::IsEqual(const ai::Search::State * const newState) const
		{
			const State * const state = dynamic_cast<const State * const>(newState);
			return (*this) == (*state);
		}

		double State::GetX() const
		{
			return this->mX;
		}
		double State::GetY() const
		{
			return this->mY;
		}
		double State::GetZ() const
		{
			return this->mZ;
		}
		double State::GetCharge() const
		{
			return this->mCharge;
		}
		double State::GetHP() const
		{
			return this->mHP;
		}
	
	
		void State::SetX(const double &newX)
		{
			this->mX = newX;
		}
		void State::SetY(const double &newY)
		{
			this->mY = newY;
		}
		void State::SetZ(const double &newZ)
		{
			this->mZ = newZ;
		}
		void State::SetCharge(const double &newCharge)
		{
			this->mCharge = newCharge;
		}
		void State::SetHP(const double &newHP)
		{
			this->mHP = newHP;
		}
	
		bool State::Display(std::ostream &os) const
		{
			os << "Pos: "
			   << this->mX << ","
			   << this->mY << ","
			   << this->mZ << " "
			   << "Charge: "
			   << this->mCharge << " "
			   << "HP: "
			   << this->mHP;
			return true;
		}
	
		std::ostream &operator<<(std::ostream &os, const State &rhs)
		{
			rhs.Display(os);
			return os;
		}
	}
}
