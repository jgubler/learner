#include <ai_search.h>
#include <cmath>
#include <iostream>
#include "jagAction3.h"
#include "jagModel3.h"
#include "jagState3.h"
#include "jagProblem3.h"

namespace jag
{
	namespace bb
	{
		Problem::Problem(ai::Search::State *init_state, Model *model_in, bool home)
			: ai::Search::Problem(init_state), mModel(model_in), mGoalState(0), mGoingHome(home)
		{
		}

		Problem::~Problem()
		{
			if(this->mGoalState)
			{
				delete this->mGoalState;
				this->mGoalState = 0;
			}
		}	

		bool Problem::GoalTest(const ai::Search::State * const new_state) const
		{
            const State * const state = dynamic_cast<const State * const>(new_state);
		    if (this->mGoingHome)
            {
                
			    if (*(this->mGoalState) == *state)
			    {
				    std::cout << "found goal"<< std::endl;
			    }
			    return *(this->mGoalState) == *state;
            }
            double x = state->GetX();
            double y = state->GetY();
            if (!this->mModel->GetCellVisited(x,y))
            {
                return true;
            }
            return false;
            
            
		}

		bool Problem::FindSuccessors(const ai::Search::State * const new_state, 
									std::vector<ai::Search::ActionStatePair>
									&results) const
		{
			const State * const state = dynamic_cast<const State * const>(new_state);
			//4 directions
			//in order of NORTH, SOUTH, EAST, WEST

			int dx[4] = {0,0,1,-1};
			int dy[4] = {1,-1,0,0};
			double newX, newY, newCharge;
			Cell newCell;
			Cell currCell = this->mModel->GetCell(state->GetX(), state->GetY());
			int terrain;
			bool success = false;
			double charge_cost;
			for(int direction = 1+(int)D_MIN; direction < D_MAX ; direction++)
			{
				terrain = currCell.GetTerrain(direction);
				switch(terrain)
				{
					case I_MUD:
						charge_cost = 2.0;
						success = true;
						break;
					case I_PLAIN:
						charge_cost = 1.0;
						success = true;
						break;
					case I_ICE:
						charge_cost = 10.0;
						success = true;
						break;
					
					case I_CLIFF:
					case I_ROCKS:
					case I_WALL:
					default:
						success = false;
						break;
				}
				if(!success)
				{
				
					continue;
				}
			
				newX = state->GetX() + 1000.*dx[direction];
				newY = state->GetY() + 1000.*dy[direction];
				newCharge = state->GetCharge() - charge_cost;
				newCell = this->mModel->GetCell(newX,newY);
				if(newCell == Cell::None())
				{
					continue;
				}
				else
				{
				
					State *newState = new State(*state);
					Action *newAction = new Action();
				
					newState->SetX(newCell.GetX());
					newState->SetY(newCell.GetY());
					newState->SetZ(newCell.GetZ());
					newState->SetCharge(newCharge);
				
					newAction->SetType(((int)Action::A_NORTH) + direction);
				
					ai::Search::ActionStatePair asp(newState, newAction);
					results.push_back(asp);
				}
			}
			return(results.size() > 0);
		}

		double Problem::StepCost(const ai::Search::State * const new_state1,
					 const ai::Search::Action * const new_action,
					 const ai::Search::State * const new_state2) const
		{
			const State * const state1 = dynamic_cast<const State * const>(new_state1);
			const Action * const action = dynamic_cast<const Action * const>(new_action);
			const State * const state2 = dynamic_cast<const State * const>(new_state2);
		
			double charge_cost = 1.0;
			int type = action->GetType();
			if(type >= Action::A_NORTH && type <= Action::A_WEST)
			{
				Cell curr_cell = this->mModel->GetCell(state1->GetX(), state1->GetY());
				int direction = type;
				int terrain = curr_cell.GetTerrain(direction);
				if(terrain == I_MUD)
				{
					charge_cost = 2.0;
				}
				if(terrain == I_ICE)
				{
					charge_cost = 10.0;
				}
			}
			else
			{
				// later
			}
			double elevation_cost = (state2->GetZ() - state1->GetZ())/1000.0;
		
			double total_cost = elevation_cost + charge_cost;
		
			return total_cost;
		}

        double Problem::Heuristic(const ai::Search::State * const new_state) const
        {
            const State * const state = dynamic_cast<const State * const>(new_state);
		    double guess;
            double x = state->GetX();
            double y = state->GetY();
            
            double dx = std::fabs(x);
            double dy = std::fabs(y);
            guess = dx + dy;
            
            return guess;
            
        }

	

		void Problem::SetGoal(State * newGoalState)
		{
			this->mGoalState = newGoalState;
		}
	}
}
