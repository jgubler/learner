#ifndef _JAGMODEL3_H_
#define _JAGMODEL3_H_

#include <string>
#include <iostream>
#include <map>
#include <utility>

namespace jag
{
	namespace bb
	{
		enum
	      {
			//keep these in order
		I_MIN   = -1,
		I_CLIFF = 0,
		I_MUD   = 1,
		I_PLAIN = 2,
		I_ROCKS = 3,
		I_WALL  = 4,
		I_ICE   = 5,
		I_ND    = 6,
		I_MAX   = 7,
		I_OUT
	      };
	    enum
	      {
			// remember n,s,e,w
		D_MIN   = -1,
		D_NORTH = 0,
		D_SOUTH = 1,
		D_EAST  = 2,
		D_WEST  = 3,
		D_MAX   = 4,
		D_OUT
	      };

		class Cell
		{
		public:
			Cell();
			Cell(const Cell &rhs);
			Cell(const double &x, const double &y, const double &z, 
				 const int &north, const int &south, const int &east, 
				 const int &west, const int &visited = 0);
			~Cell();
		
			Cell &operator=(const Cell &rhs);
			bool operator==(const Cell &rhs) const;

			void SetVisited(const bool visited);
			int SetTerrain(const int &direction, const int &terrain);
		
			double GetX() const;
			double GetY() const;
			double GetZ() const;
			int GetTerrain(const int &direction) const;
			bool GetVisited() const;
		
			static const Cell None();
		
			static int TerrainLabelToEnum(const char *s);
			static const char *TerrainEnumToLabel(const int &num);
		
			bool Display(std::ostream &os) const;
		
		protected:
			double mX, mY;
			double mZ;
			bool mVisited;
			int terrains[4];
		private:
		};
		std::ostream &operator<<(std::ostream &os, const Cell &rhs);

        class Object
        {
        public:
            Object();
            Object(std::string size, std::string shape, std::string color, std::string id, int points);
            ~Object();
    
            std::string GetSize();
            std::string GetShape();
            std::string GetColor();
            std::string GetId();
            int GetPoints();
            
            void SetSize(std::string size);
            void SetShape(std::string shape);
            void SetColor(std::string color);
            void SetId(std::string id);
            void SetPoints(int points);
            
        protected:
            std::string mSize;
            std::string mShape;
            std::string mColor;
            std::string mId;
            int mPoints;
            
        private:
        };
	
		class Model
		{
		public:
			Model();
			~Model();
			double GetCharge() const;
			double GetHP() const;
			double GetXloc() const;
			double GetYloc() const;
			double GetZloc() const;
			double GetGX() const;
			double GetGY() const;
			double GetGZ() const;
            int GetCellTerrain(const double &x, const double &y, const int &direction);
            bool GetCellVisited(const double &x, const double &y);
		
			Cell GetCell(const double &x, const double &y);
			bool AddCell(const Cell &cell);
		
			void SetCharge(const double &charge);
			void SetHP(double &hp);
			void SetXloc(const double &x);
			void SetYloc(const double &y);
			void SetZloc(const double &z);
			void SetGoalLoc(const double &gx, const double &gy, const double &gz);
            void SetCellTerrain(const double &x, const double &y, const int &direction, const int &terrain);
            void SetCellVisited(const double &x, const double &y,const bool &visited);
		
			bool Display(std::ostream &os) const;
		protected:
			std::map<std::pair<double,double>,Cell> mCells;
            double mXLoc, mYLoc, mZLoc;            
            double mGX, mGY, mGZ;
			double mCharge, mHP;
			
			
		private:
		};
		std::ostream &operator<<(std::ostream &os, const Model &rhs);
	}
}

#endif /* _jagModel3_H_ */
