#ifndef _JAGSTATE3_H_
#define _JAGSTATE3_H_
#include <ai_search.h>

namespace jag
{
	namespace bb
	{
		class State : public ai::Search::State
		{
		public:
			State();
			State(const State &rhs);
			virtual ~State();
		
			State &operator=(const State &rhs);
			bool operator==(const State &rhs) const;

			virtual void Display() const;
			virtual bool IsEqual(const ai::Search::State * const newState) const;

			double GetX() const;
			double GetY() const;
			double GetZ() const;
			double GetCharge() const;
			double GetHP() const;
		
			void SetX(const double &newX);
			void SetY(const double &newY);
			void SetZ(const double &newZ);
			void SetCharge(const double &newCharge);
			void SetHP(const double &newHP);

			bool Display(std::ostream &os) const;
		
		protected:
			double mX,mY,mZ;
			double mCharge;
			double mHP;
		private:
		};
		std::ostream &operator<<(std::ostream &os, const State &rhs);
	}
}

#endif /* _jagState3_h_ */
