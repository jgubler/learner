#ifndef _JAGPROBLEM3_H_
#define _JAGPROBLEM3_H_
#include <vector>
#include <ai_search.h>
#include "jagModel3.h"
#include "jagState3.h"

namespace jag
{
	namespace bb
	{
		class Problem : public ai::Search::Problem
		{
		public:
			Problem(ai::Search::State *init_state, Model *model_in, bool home = false);
			virtual ~Problem();

			virtual bool GoalTest(const ai::Search::State * const new_state) const;


			virtual bool FindSuccessors(const ai::Search::State * const new_state, 
										std::vector<ai::Search::ActionStatePair>
										&results) const;
			virtual double StepCost(const ai::Search::State * const new_state1,
						const ai::Search::Action * const new_action,
						const ai::Search::State * const new_state2) const;
            virtual double Heuristic(const ai::Search::State * const new_state) const;

			void SetGoal(State * newGoalState);
		protected:
			Model *mModel;
			State *mGoalState;
            bool mGoingHome;
		private:
		};
	}
}

#endif /* _jagProblem3_h_ */
